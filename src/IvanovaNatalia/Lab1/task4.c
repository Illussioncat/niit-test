#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>


int main()
{
    int foot = 0;
    int inch = 0;
    float cm;
    
    printf("Please, enter your growth on form foot.inch\n");
    if (scanf("%d'%d", &foot, &inch) == 2)
       {
           inch = inch + foot * 12;
           cm = inch*2.54f;
           printf("%.2f\n", cm);
       }
        
    else puts("Error!");
    return 0;
}